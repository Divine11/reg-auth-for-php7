<?php
session_start();
require_once 'include/safemysql.class.php';
$data = $_POST;
$db = new SafeMySQL();
if (isset($_SESSION['id'])) {
    header("Location: index.php");
}
if (isset($data['register'])) {
    $errors = array();
    if (trim($data['login']) == '') {
        $errors[] = 'Введите логин';
    }
    $errors = array();
    if (trim($data['password']) == '') {
        $errors[] = 'Введите пароль';
    }
    $select = $db->getRow("SELECT * FROM userinfo WHERE pLogin = ?s", $data['login']);
    if ($data['password_2'] == $data['password']) {
        if (empty($errors)) {
            if (!$select) {
                $db->query(
                    "INSERT INTO `userinfo`(`pLogin`, `pPassword`) 
                VALUES (?s, ?s)",
                    $data['login'],
                    password_hash($data['password'], PASSWORD_DEFAULT)
                );
                header("location: /index.php");
            } else {
                echo 'Такой логин уже используется.';
            }
        }
    } else {
        'Повторный пароль введен не вверно';
    }
};
?>

<html>
<form  method="POST">
    <strong>Ваш логин</strong>
    <input type="text" name="login" ><br>

    <strong>Ваш пароль</strong>
    <input type="password" name="password" <br>
    <strong>Повтор пароля</strong>
    <input type="password" name="password_2" <br>
    <button type="submit" name="register">Регистрация</button>
</form>
</html>
