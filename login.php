<?php
session_start();
require_once 'include/safemysql.class.php';
$data = $_POST;
$db = new SafeMySQL();
if (isset($_SESSION['id'])) {
    header("Location: index.php");
}
if (isset($data['auth'])) {
    $nick = $_POST['login'];
    $password = $_POST['password'];

    $user = $db->getRow("SELECT * FROM userinfo WHERE pLogin = ?s", $nick);
    if ($user) {
        if (password_verify($data['password'], $user['pPassword'])) {
            //если пароль совпадает, то нужно авторизовать пользователя
            $_SESSION['id'] = $user['id'];
            header("Location: index.php");
        } else {
            echo  '<div style="color:darkred;">Логин или пароль введен не верно</div><hr>';
        }
    }
    if (!$user) {
        echo '<div style="color:darkred;">Логин или пароль введен не верно</div><hr>';
    }
};
?>
<html>
<form  method="POST">
    <strong>Логин</strong>
    <input type="text" name="login" ><br>

    <strong>Пароль</strong>
    <input type="password" name="password"><br>

    <button type="submit" name="auth">Войти</button>
</form>
</html>
